﻿using System;
using DemoApi.Entities;
using Microsoft.EntityFrameworkCore;

namespace DemoApi.Data
{
    public class EmployeeDbContext : DbContext
    {
        public DbSet<Employee> Employees { get; set; }

        public DbSet<Address> Addresses { get; set; }

        public DbSet<Department> Departments { get; set; }

        public EmployeeDbContext(DbContextOptions<EmployeeDbContext> dbContextOptions):base(dbContextOptions)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //Employee
            modelBuilder.Entity<Employee>().HasKey(x => x.EmployeeId);

            // Address
            modelBuilder.Entity<Address>().HasKey(x => x.AddressId);
            modelBuilder.Entity<Address>().HasOne(x => x.Employee).WithMany(x => x.Addresses).HasForeignKey(x => x.EmployeeId);

            //Department
            modelBuilder.Entity<Department>().HasKey(x => x.DepartmentId);
            modelBuilder.Entity<Department>().HasOne(x => x.CreatedByEmp).WithMany(x => x.CreatedByMeDepartments).HasForeignKey(x => x.CreatedBy).OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<Department>().HasOne(x => x.ModifiedByEmp).WithMany(x => x.ModifiedByMeDepartments).HasForeignKey(x => x.ModifiedBy).OnDelete(DeleteBehavior.Restrict);
        }
    }
}
