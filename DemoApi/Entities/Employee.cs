﻿using System;
using System.Collections.Generic;

namespace DemoApi.Entities
{
    public class Employee
    {
        public Guid EmployeeId { get; set; }

        public string Name { get; set; }

        public ICollection<Address> Addresses { get; set; }

        public ICollection<Department> CreatedByMeDepartments { get; set; }

        public ICollection<Department> ModifiedByMeDepartments { get; set; }

        public Employee()
        {

        }
    }
}
