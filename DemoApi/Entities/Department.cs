﻿using System;
namespace DemoApi.Entities
{
    public class Department
    {
        public Guid DepartmentId { get; set; }

        public string DepartmentName { get; set; }

        public Guid CreatedBy { get; set; }

        public Employee CreatedByEmp { get; set; }

        public Guid ModifiedBy { get; set; }

        public Employee ModifiedByEmp { get; set; }

        public DateTime CreatedDate { get; set; }

        public Department()
        {
        }
    }
}
