﻿using System;
namespace DemoApi.Entities.Enumeration
{
    public enum AddressEnum
    {
        Permanent = 1,
        Current
    }
}
