﻿using System;
namespace DemoApi.Entities
{
    public class Address
    {
        public Guid AddressId { get; set; }

        public int HouseNumber { get; set; }

        public int StreetName { get; set; }

        public int AddressType { get; set; }

        public Guid EmployeeId { get; set; }

        public Employee Employee { get; set; }

        public Address()
        {
        }
    }
}
